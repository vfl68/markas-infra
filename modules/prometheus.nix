{ pkgs, config, ...}:

let
  # This is to add a label instance sets with the address as value
  relabel_configs = [
    {
      source_labels = [ "__address__" ];
      target_label = "instance";
      regex = "([^:]+).*";
    }
  ];
in
{
  networking.firewall = {
    extraCommands = ''
      # To allow VPN members to connect to the grafana service
      iptables -I INPUT -s 10.100.0.1/24 -p tcp --dport 9090 -i wg-member -j ACCEPT
      iptables -I INPUT -s 10.100.0.1/24 -p tcp --dport 9100 -i wg-member -j ACCEPT
      iptables -I INPUT -s 10.100.0.1/24 -p tcp --dport 9093 -i wg-member -j ACCEPT
    '';
  };

  services.prometheus = {
    enable = true;
    scrapeConfigs = [
      {
        inherit relabel_configs;
        job_name = "prometheus";
        scrape_interval = "5s";
        static_configs = [{ targets = ["localhost:9090"]; }];
      }
      {
        inherit relabel_configs;
        job_name = "node";
        scrape_interval = "15s";
        static_configs = [{ targets = ["localhost:9100"]; }];
      }
    ];
    # TODO: we need to add more rules /:
    ruleFiles = [ ../conf/prometheus/node.rules ];
    # TODO: we need to define exporters we want
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
      };
    };

    alertmanagers = [
      {
        static_configs = [
          { targets = [ "localhost:9093" ]; }
        ];
      }
    ];

    alertmanager = {
      enable = true;
      configuration = {
        global = {
          smtp_from = "monitoring@markas.fr";
          smtp_smarthost = "localhost:25";
        };
        receivers = [
          {
            name = "mail";
            email_configs = [
              {
                to = "lewo@abesis.fr";
                tls_config.insecure_skip_verify = true;
              }
            ];
          }
        ];
        route.receiver = "mail";
      };
    };
  };

  systemd.services.test-weekly-alertmanager = {
    description = "Weekly Alertmanager test";
    wantedBy = [ "multi-user.target" ];
    wants = [ "alertmanager.service" ];
    after = [ "alertmanager.service" ];
    restartIfChanged = false;
    unitConfig.X-StopOnRemoval = false;
    startAt = "weekly";
    path = [ pkgs.bash pkgs.curl pkgs.netcat ];
    script = ''
      timeout 10 bash -c 'until nc -z localhost 9093; do sleep 1; done'

      curl -H "Content-Type: application/json" \
           -d '[{"labels":{"alertname":"TestWeeklyAlertmanager"}}]' \
           localhost:9093/api/v1/alerts
    '';
  };
}
