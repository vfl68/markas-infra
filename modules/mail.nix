{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.mailserver.check;
in
{
  options = {
    mailserver.check = {
      enable = mkEnableOption "check-mail";
      imapHost = mkOption {
        type = types.str;
      };
      dstAddr = mkOption {
        type = types.str;
      };
      dstPasswordFile = mkOption {
        type = types.str;
      };
      ignoreDkimSpf = mkOption {
        type = types.bool;
        default = false;
      };
    };
  };
  config = mkIf cfg.enable {
    systemd.services.check-mail = {
      wantedBy = [ "multi-user.target" ];
      wants = [ "postfix.service" "dovecot2.service" ];
      after = [ "postfix.service" "dovecot2.service" ];
      startAt = "daily";
      serviceConfig = {
        Type = "oneshot";
      };
      script = ''
        ${pkgs.python3}/bin/python ${../scripts/mail-check.py} \
          send-and-read \
          --smtp-host localhost \
          --from-addr registration@markas.fr \
          --imap-host ${cfg.imapHost} \
          --to-addr ${cfg.dstAddr} \
          --dst-password-file ${cfg.dstPasswordFile} \
          ${if cfg.ignoreDkimSpf then "--ignore-dkim-spf" else ""}
        '';
    };
    keys.mailserver-check-dst-password.path = cfg.dstPasswordFile;
  };
}
