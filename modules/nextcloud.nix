{ pkgs, lib, config, ...}:

with lib;
with builtins;

let
  cfg = config.nextcloud;

  addNivApp = nivApp: ''
    ${pkgs.coreutils}/bin/cp -ar ${nivApp.outPath} $out/apps/${nivApp.name}
  '';

  addNivApps = concatStrings (map addNivApp cfg.apps);

  enableNivApps = map (app: ''
    nextcloud-occ app:enable ${app.name}
  '') cfg.apps;

  # Cleanup override info
  settings = pkgs.lib.mapAttrsRecursiveCond
    (s: ! s ? "_type")
    (_: value: if value ? "content" then value.content else value)
    cfg.settings;

  addUsers = let
    userPasswdPath = name: ''${config.keys."nextcloud-${name}".path}'';
    addUser = name: ''
      export OC_PASS=$(cat ${userPasswdPath name})
      ${pkgs.sudo}/bin/sudo -u nextcloud --preserve-env=OC_PASS -- \
        nextcloud-occ user:add \
          --group admin \
          --password-from-env \
          ${name}
      # If the user is already created, we may want to update its
      # password.
      ${pkgs.sudo}/bin/sudo -u nextcloud --preserve-env=OC_PASS -- \
        nextcloud-occ user:resetpassword \
          --password-from-env \
          ${name}
      unset OC_PASS
      '';
  in concatMapStringsSep "\n" addUser cfg.adminUsers;

  # Generate key entries for all admin users
  # Each generated key looks like:
  #   keys.nextcloud-lewo = {
  #     path = "/run/keys/nextcloud-lewo";
  #     user = "nextcloud";
  #   };
  userKeys = let
    props = user: {
      path = "/var/keys/nextcloud-${user}";
      user = "nextcloud";
    };
    pairs = map (user: nameValuePair "nextcloud-${user}" (props user)) cfg.adminUsers;
  in listToAttrs pairs;

  # TODO: we need to add a package attribute to the nextcloud module instead.
  nextcloud = pkgs.nextcloud18.overrideAttrs(oldAttrs: rec {
    postFixup = ''
      ${addNivApps}
    '';
  });

in {

  options.nextcloud = {

    enable = mkEnableOption "Enable nextcloud";

    apps = mkOption {
      type = types.listOf types.path;
      default = [];
      description = "
        List of Nextcloud apps to enable
      ";
    };

    adminUsers = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "
        List of admin users to create
      ";
    };

    settings = mkOption {
      type = types.attrsOf types.attrs;
      default = {};
      description = "
        Nextcloud settings to be imported using `occ config:import`

        https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/occ_command.html#config-commands
      ";
    };

  };

  config = mkIf cfg.enable {

    services.mysql = {
      enable = true;
      package = pkgs.mysql;
      ensureDatabases = [ "nextcloud" ];
      ensureUsers = [
        {
          name = "nextcloud";
          ensurePermissions = {
            "nextcloud.*" = "ALL PRIVILEGES";
          };
        }
      ];
    };

    keys = userKeys // {
      # This is the nextcloud admin password
      nextcloud  = {
        path = "/var/keys/nextcloud";
        user = "nextcloud";
      };
    };

    # The port 80 is required by acme
    networking.firewall.allowedTCPPorts = [ 443 80 ];

    services.nginx = {
      enable = true;
      virtualHosts = {
        "cloud.markas.fr" = {
          default = true;
          forceSSL = true;
          enableACME = true;
        };
      };
    };

    services.nextcloud = {
      enable = true;
      package = nextcloud;
      logLevel = 0;
      https = true;
      hostName = "cloud.markas.fr";
      nginx.enable = true;
      caching = {
        apcu = true;
      };
      config = {
        dbtype = "mysql";
        adminuser = "admin";
        adminpassFile = config.keys.nextcloud.path;
      };
    };

    # This is to do the initial nextcloud setup only when Mysql and
    # Redis are ready. We need to add this becaux mysql is on the same
    # host.
    systemd.services.nextcloud-setup = {
      serviceConfig.RemainAfterExit = true;
      partOf = [ "phpfpm-nextcloud.service" ];
      after = map (name: "nextcloud-${name}-key.service") cfg.adminUsers
              ++ [ "nextcloud-key.service" "mysql.service"];
      requires = map (name: "nextcloud-${name}-key.service") cfg.adminUsers
                 ++ [ "nextcloud-key.service" "mysql.service" ];
      script = mkAfter ''
        ${addUsers}
        nextcloud-occ user:setting admin settings email admin@${config.networking.domain}
        ${concatStringsSep "\n" enableNivApps}
        echo '${toJSON settings}' | nextcloud-occ config:import
        # This is required to enable apps :/
        nextcloud-occ upgrade
      '';
    };

  };

}
