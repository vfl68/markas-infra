{ config, pkgs, sources, ... }: {
  imports = [
    ./hardware-configuration.nix
    ./modules/prometheus.nix
    ./modules/auto-upgrade.nix
    ./modules/vpn.nix
    ./modules/grafana.nix
    ./modules/nextcloud.nix
    ./modules/documentation.nix
    ./modules/mail.nix
  ];

  time.timeZone = "Europe/Paris";

  services.journald.enableHttpGateway = true;

  networking.domain = "markas.fr";

  services.vpn.member = {
    privateKeyFile = "/var/keys/wireguard-member";
    clients = [
      {
        # lewo's vpn
        ip = "10.100.0.2";
        publicKey = "LL06/OugJg7Wht8fA3dnCoTpLAPfg7/QZqqwdueNT10=";
      }
      {
        # relaxmax68's vpn
        ip = "10.100.0.3";
        publicKey = "a/cY89F92iE9JF1Ki8Ks/C0h5DUKxBfFWNu8BFFzfHc=";
      }
    ];
  };

  security.acme.email = "contact@markas.fr";
  security.acme.acceptTerms = true;

  services.openssh = {
      enable = true;
      passwordAuthentication = false;
  };

  services.autoUpgrade = {
    enable = true;
    remotes = [
      "https://framagit.org/markas/infrastructure.git"
      # We can fallback to GitHub if framagit is not responding anymore
      "https://github.com/markas-fr/infrastructure.git"
      # And to a local folder if GitHub is not working:/
      "/tmp/markas-infrastructure"
    ];
    keys = [
      ./keys/lewo.asc
      ./keys/relaxmax68.asc
    ];
  };

  environment.systemPackages = with pkgs; [
    smartmontools tmux htop emacs26-nox
  ];

  mailserver = {
    enable = true;
    fqdn = "mail.markas.fr";
    domains = [ "markas.fr" ];
    check = {
      enable = true;
      imapHost = "imap.gmail.com";
      dstAddr = "markas.monitoring@gmail.com";
      dstPasswordFile = "/var/keys/mailserver-dst-password";
    };
  };

  nextcloud = {
    enable = true;
    adminUsers = [ "lewo" "relaxmax" ];
    apps = with sources; [
      nextcloud-calendar
      nextcloud-contacts
      nextcloud-registration
    ];
    settings = {
      system = {
        appstoreenabled = false;
        mail_smtphost = "127.0.0.1:25";
        mail_domain = "markas.fr";
      };
      apps.core.backgroundjobs_mode = "cron";
      apps.registration.admin_approval_required = "yes";
    };
  };

  users.extraUsers.lewo = {
    isNormalUser = true;
    home = "/home/lewo";
    description = "Antoine Eiche";
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBQglK0rutCsTPE9s4g/HkcIZbNZbUjwhOZQ8jHNqVkkUFsl+V3sbekOYSqGnCfqnWthf+L7885COPGTFP5Yg2f7WGaExH7CD1vlsd9bQ1VPQWFzhCXrysaazcYMW/Aggy3RHjNslNW8UOvFa1ccszDz21U0NWmOivFmbhaU2CrUeal3bLuDiIDGP2FSKdkL23d/eMCo63SEsjuMCvXEYqu5zurUoNjmp/buVx3aYZDQh+xnydeN97/O//KFPISCdzDy6gnXKDvQqOqZEI3CsHNlNEf99sOMwZg4xGMXkJ82oDDxUB8bvYn64YmZsFzJO4ciLNat2bLkUHIJLm2oaZ lewo@none" ];
    # Don't know how this is supposed to work:/ So, ATM, we manually
    # have to set a password on the server
    # passwordFile = "/var/secrets/user-lewo";
  };

  users.extraUsers.relaxmax = {
    isNormalUser = true;
    home = "/home/relaxmax";
    description = "Maxime Frieh";
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9waKuNqd6oyJpPdYjYolRxYj1zpSPp2QbViC08i4+pI+OszQbmd+ElDyC7F5l4W+xbDHE1NhxBzlMspQFKlIuewtosOtgQRLgfjX8g+nwi8ZeoYxeKaIawfwdHJkIBYI90se/HCQ9CxyUqWtdxI4peM91dijEZVMgMlBpWl2eUTsBRi/8DIiIRL8qcCDdRF5Fb6u6TY3YsGgFeIgFy2/BARilnr6Z34HD287M6yGSptYVVxjfzLsApf0JVaR17ud7zhTlcFyzYX0l+fE2MBRr0cKivP7Y6Reoxjyj9+COkDsu9rwld7uZZ8FA15OpegQJRCAw9AhE+NgIQSl6diCMWYakMZCyWMuXcZswpTt2VS3nKEpOFefTCrn7T2TVCGL4WOPrQ97NjveGCuIYSPoMwdD/h5Q+REowidYCbIZGiH5t/neXtSX2a1vwHTz12IUDWEbHXrvzd3Y4bLXMHGAqOAPmW6rTcuXymCals8vZm4dS2/cZS/AtYYTG81KmLqdE95D5bCcMSOMdCzyWjIf6dfHk0TmJ9MO2UAkGF4gcM0Oa595eUO2/LyG4zXD9MRG+Rl8ytQoD+66QTwrNG0eX5+4etna0MR1j6fpBU/ZItOgV6Ydp3x5Rt3mfMnziJyXxYvtIJp86Us0NT/cSqBrgfovLCD2Tx0APN+du1V6APQ==" ];
  };
}
