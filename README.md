Ce dépot contient la configuration du serveur hébergeant les services
de l'infrastructure `markas.fr`.

Cette infrastructure est en version beta et ne doit donc pas être
considérée comme stable.

La documentation est disponible [ici](https://documentation.markas.fr).
