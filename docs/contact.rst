Nous contacter
--------------

Par messagerie instantanée
==========================

Canal #markas sur irc.geeknode.org (IRC) ou avec un navigateur en
cliquant ici : `chat de Markas
<https://kiwiirc.com/client/irc.geeknode.org/markas/>`_.

Note : n'hésitez pas à poser vos questions, mais les gens ne sont pas forcément derrière un écran au moment où vous posez vos questions, il est donc possible que vous attendiez un moment avant qu'une personne réagisse…

Par courriel
============

contact (a) markas (point) fr
