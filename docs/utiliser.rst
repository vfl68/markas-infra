Utiliser les services de markas.fr
----------------------------------

Nextcloud
~~~~~~~~~

Nexcloud est un logiciel libre permettant notament l'hébergement de
fichiers, la gestion de calendriers et de contacts. Ce service est
disponible à l'addresse https://cloud.markas.fr et requiert un compte
utilisateur.

Création d'un compte utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La création de compte est actuellement manuelle. Prennez contact avec
contact@markas.fr.
