.. Markas documentation master file, created by
   sphinx-quickstart on Sun Apr  5 15:58:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur l'infrastructure markas.fr
========================================

.. warning::

   L'infrastructure markas.fr est actuellement en version beta et ne doit
   pas être considérée comme stable!

.. toctree::
   :maxdepth: 2

   utiliser.rst
   description.rst
   contribuer.rst
   administration.rst
   contact.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
