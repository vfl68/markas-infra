{ pkgs, configuration, sources }:

with import (pkgs.path + /nixos/lib/testing-python.nix) { system = builtins.currentSystem; };
with import ./lib.nix { inherit pkgs; };
with pkgs.lib;

let

  jq = "${pkgs.jq}/bin/jq";
  password = "recipient";

in makeTest {
  name = "test-prometheus";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ configuration ];

      config = {
        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 1 * 1024;
        };
        # This service is used to validate a prometheus alert is fired
        # when a systemd service failed.
        systemd.services.unitFailed = {
          wantedBy = [ "multi-user.target" ];
          script = "false";
        };

        mailserver = {
          loginAccounts = {
            "recipient@markas.fr" = {
              hashedPasswordFile = hashPassword password;
            };
          };
          enableImapSsl = true;
          localDnsResolver = false;
          check.enable = mkForce false;
        };

        services.prometheus.alertmanager.configuration = {
          receivers = [
            {
              name = "mail-test";
              email_configs = [
                {
                  to = "recipient@markas.fr";
                  tls_config.insecure_skip_verify = true;
                }
              ];
            }
          ];
          route.receiver = "mail-test";
        };

        # It takes too much resource
        services.autoUpgrade.enable = pkgs.lib.mkForce false;
        services.grafana.enable = pkgs.lib.mkForce false;

        # To output logs to check alertmanager is trying to send a mail
        services.prometheus.alertmanager.logLevel = "debug";
      };
    };
  };

  testScript = ''
    machine.wait_for_unit("prometheus.service")
    # Prometheus takes a bit of time to expose these job values
    machine.wait_until_succeeds(
        'curl -L localhost:9090/api/v1/label/job/values | \
        ${jq} -e \'.data | sort | . == ["node", "prometheus"]\'''
    )

    machine.wait_until_succeeds(
        'curl -s -L localhost:9090/api/v1/alerts | \
        ${jq} -e \'.data.alerts[] | select(.labels.name=="unitFailed.service") | .state == "firing"\'''
    )

    # Check if the alert manager gets the alert.
    # Note the alertmanager takes around 30sec to be notified.
    machine.wait_until_succeeds(
        "curl localhost:9093/api/v2/alerts | \
        ${jq} -e '.[] | select(.labels.name == \"unitFailed.service\")'"
    )

    machine.wait_until_succeeds(
        "${mailCheck} read --ignore-dkim-spf --imap-username recipient@markas.fr --imap-password ${password} FIRING"
    )
  '';
}
