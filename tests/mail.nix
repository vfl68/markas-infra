{ pkgs, configuration }:

with import (pkgs.path + /nixos/lib/testing-python.nix) { system = builtins.currentSystem; };
with import ./lib.nix { inherit pkgs; };

let
  password = "recipient";

in

makeTest {
  name = "test-mail";
  nodes = {
    markas = {pkgs, config, ...}: {
      imports = [ configuration ];
      config = {
        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 1 * 1024;
        };
        # Speedup the test by removing useless services
        services.autoUpgrade.enable = pkgs.lib.mkForce false;
        services.grafana.enable = pkgs.lib.mkForce false;
        services.nextcloud.enable = pkgs.lib.mkForce false;

        mailserver = {
          loginAccounts = {
            "recipient@markas.fr" = {
              hashedPasswordFile = hashPassword password;
            };
          };
          enableImap = true;
          enableImapSsl = true;
          check.imapHost = pkgs.lib.mkForce "localhost";
          check.dstAddr = pkgs.lib.mkForce "recipient@markas.fr";
          # Because DKIM and SPF are not available locally
          check.ignoreDkimSpf = true;
          check.dstPasswordFile = pkgs.lib.mkForce (toString (pkgs.writeTextFile {
            name = "check.dstPassowrdFile";
            text = password;
          }));
        };
      };
    };
  };

  testScript = ''
    markas.wait_for_unit("multi-user.target")
    markas.wait_until_succeeds(
        "journalctl -u check-mail.service | grep -q 'has been found'"
    )
  '';
}
