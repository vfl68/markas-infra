{ pkgs, configuration }:

with import (pkgs.path + /nixos/lib/testing-python.nix) { system = builtins.currentSystem; };
with import ./lib.nix { inherit pkgs; };
with pkgs.lib;

let
  adminPassword = "admin";
  memberPassword = "deiph1shee5eiSa";
in

makeTest {
  name = "test-nextcloud";
  nodes = {
    machine = {pkgs, config, ...}: {
      imports = [ configuration ];
      config = {
        virtualisation = {
          diskSize = 1 * 1024;
          memorySize = 2 * 1024;
        };

        # Speedup the test by removing useless services
        services.autoUpgrade.enable = mkForce false;
        services.grafana.enable = mkForce false;

        # We create values for expected Nextcloud passwords
        keys.nextcloud.path = mkForce ''${pkgs.writeTextFile {
          name = "nextcloud.password";
          text = "admin";
        }}'';

        networking.extraHosts = "127.0.0.1 cloud.markas.fr";

        nextcloud = {
          adminUsers = mkForce [];
        };

        mailserver = {
          loginAccounts = {
            "admin@markas.fr" = {
              hashedPasswordFile = hashPassword adminPassword;
            };
            "member@markas.fr" = {
              hashedPasswordFile = hashPassword memberPassword;
            };
          };
          enableImapSsl = true;
          localDnsResolver = false;
          check.enable = mkForce false;
        };

        environment.systemPackages = with pkgs; [
          firefox-unwrapped geckodriver
        ];

      };
    };
  };

  testScript = let

    withRcloneEnv = pkgs.writeScript "with-rclone-env" ''
      #!${pkgs.stdenv.shell}
      export RCLONE_CONFIG_NEXTCLOUD_TYPE=webdav
      export RCLONE_CONFIG_NEXTCLOUD_URL="https://cloud.markas.fr/remote.php/webdav/"
      export RCLONE_CONFIG_NEXTCLOUD_VENDOR="nextcloud"
      export RCLONE_CONFIG_NEXTCLOUD_USER="member"
      export RCLONE_CONFIG_NEXTCLOUD_PASS="$(${pkgs.rclone}/bin/rclone obscure ${memberPassword})"
      "''${@}"
    '';

    copySharedFile = pkgs.writeScript "copy-shared-file" ''
      #!${pkgs.stdenv.shell}
      echo 'hi' | ${withRcloneEnv} ${pkgs.rclone}/bin/rclone --no-check-certificate rcat nextcloud:test-shared-file
    '';

    diffSharedFile = pkgs.writeScript "diff-shared-file" ''
      #!${pkgs.stdenv.shell}
      diff <(echo 'hi') <(${pkgs.rclone}/bin/rclone --no-check-certificate cat nextcloud:test-shared-file)
    '';

    requestAccount = pkgs.writers.writePython3Bin "request-account" {
      libraries = [ pkgs.python3Packages.selenium ];
    } ''
      from selenium.webdriver import Firefox
      from selenium.webdriver.firefox.options import Options

      options = Options()
      options.add_argument('--headless')
      driver = Firefox(options=options)

      driver.implicitly_wait(20)
      driver.get('https://cloud.markas.fr')
      assert "Nextcloud" in driver.title

      driver.find_element_by_link_text('Register').click()

      email_input = driver.find_element_by_id("email")
      email_input.send_keys("member@markas.fr")
      driver.find_element_by_id("submit").click()

      message = driver.find_elements_by_xpath("//main/ul/li")[0].text
      assert "Verification email successfully sent." == message
    '';

    createAccount = pkgs.writers.writePython3Bin "create-account" {
      libraries = [ pkgs.python3Packages.selenium ];
    } ''
      import sys

      from selenium.webdriver import Firefox
      from selenium.webdriver.firefox.options import Options

      # Verification url sent by email when someone register on nextcloud
      verify_url = sys.argv[1]

      options = Options()
      options.add_argument('--headless')
      driver = Firefox(options=options)

      driver.implicitly_wait(20)
      driver.get(verify_url)

      username_input = driver.find_element_by_id("username")
      username_input.send_keys("member")

      password_input = driver.find_element_by_id("password")
      password_input.send_keys("${memberPassword}")

      driver.find_element_by_id("submit").click()

      message = driver.find_elements_by_xpath("//main/ul/li")[0].text
      assert "still needs approval from an administrator" in message
    '';

  in ''
    machine.wait_for_unit("multi-user.target")
    machine.succeed("curl -Lk https://cloud.markas.fr/login")

    # Request a new account creation for member@markas.fr
    # Unix user member will receive a verification link by mail
    machine.succeed(
        "${requestAccount}/bin/request-account"
    )
    # Read received email in member mailbox to get verify url
    url = machine.succeed(
        "${mailCheck} read --show-body --ignore-dkim-spf --imap-username member@markas.fr --imap-password ${memberPassword} Nextcloud | grep -e ^http"
    )
    # Go to verify url and create account
    machine.succeed(
        "${createAccount}/bin/create-account %s"
        % url
    )
    # Check that admin user received notification about member user account creation
    machine.succeed(
        "${mailCheck} read --ignore-dkim-spf --imap-username admin@markas.fr --imap-password ${adminPassword} Nextcloud"
    )
    # Check member user account is disabled
    machine.succeed("nextcloud-occ user:info member | grep 'enabled: false'")
    # Enable the user
    machine.succeed("nextcloud-occ user:enable member")

    # Check webdav with member user
    machine.succeed(
        "${withRcloneEnv} ${copySharedFile}"
    )
    machine.succeed(
        "${withRcloneEnv} ${diffSharedFile}"
    )

    # TODO: check if calendar and contacts app are available
  '';
}
