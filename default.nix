let
  sources = import ./nix/sources.nix;
  nixpkgs = sources.nixpkgs;

  # This can be used to patch nixpkgs
  # nixpkgsPatched = upstreamPkgs.stdenv.mkDerivation {
  #   name = "nixpkgs-patched";
  #   src = sources.nixpkgs;
  #   patches = [ ];
  #   installPhase = "cp -r ./ $out/";
  #   fixupPhase = ":";
  # };
  # pkgs = import nixpkgsPatched {};

  pkgs = import nixpkgs {
    overlays = [ markasOverlay ];
  };

  # The server configuration
  configuration = {
    imports = [
      ./configuration.nix
      (import sources.nixos-mailserver)
    ];
    config = {
      _module.args = { inherit pkgs sources; };
    };
  };

  # The configuration of a VM looking like the server
  vmConfiguration = {
    imports = [
      ./vm.nix
      (import sources.nixos-mailserver)
    ];
    config = {
      _module.args = { inherit pkgs sources; };
    };
  };

  # Make a system from a configuration
  makeSystem = config:
    import (pkgs.path + /nixos) {
      system = "x86_64-linux";
      configuration = config;
    };

  markasOverlay = self: super: {
    markas.documentation = pkgs.callPackage ./docs {
      nixpkgsBranch = nixpkgs.branch;
      nixpkgsUrl = "https://github.com/NixOS/nixpkgs/tree/${nixpkgs.rev}";
      nixpkgsRev = nixpkgs.rev;
    };
  };

in {
  system = (makeSystem configuration).system;
  vm = (makeSystem vmConfiguration).vm;
  documentation = pkgs.markas.documentation;
  tests = {
    prometheus = pkgs.callPackage ./tests/prometheus.nix { inherit configuration sources; };
    autoUpgrade = pkgs.callPackage ./tests/auto-upgrade.nix { };
    keys = pkgs.callPackage ./tests/keys.nix { };
    vpn = pkgs.callPackage ./tests/vpn.nix { };
    full = pkgs.callPackage ./tests/full.nix { inherit configuration; };
    nextcloud = pkgs.callPackage ./tests/nextcloud.nix { inherit configuration; };
    mail = pkgs.callPackage ./tests/mail.nix { inherit configuration; };
  };
}
